import packages.Packages_Spark
import org.apache.spark.sql.functions._
//import org.apache.hadoop.fs._
//import io.delta.tables._

object Creating_SQL_DB {
  def main(args: Array[String]): Unit = {
    print("Starting Application...")

//    Getting spark session starting packages
    val creatingDF = new Packages_Spark
//    Starting SparkSession
    val spark = creatingDF.startSession

    spark.sparkContext.setLogLevel("ERROR")

//    Loading filepath into scala(spark)
    val HTData_1_path = "C:\\Users\\NIVETHAN\\Desktop\\Databricks\\health_tracker_data\\health_tracker_data_2020_01.json"
    val HTData_2_path = "C:\\Users\\NIVETHAN\\Desktop\\Databricks\\health_tracker_data\\health_tracker_data_2020_02.json"
    val HTData_3_path = "C:\\Users\\NIVETHAN\\Desktop\\Databricks\\health_tracker_data\\health_tracker_data_2020_02-01.json"

//    Reading files from the path loaded
    val HTData_1 = creatingDF.readingFile(spark,HTData_1_path,"json")
    val HTData_2 = creatingDF.readingFile(spark,HTData_2_path,"json")
    val HTData_3 = creatingDF.readingFile(spark,HTData_3_path,"json")

//    Viewing Files
    val data_1 = HTData_1.where("device_id == 0").agg(count("device_id"))
    data_1.show()
    val data_2 = HTData_1.groupBy("device_id").agg(count("device_id"))
    data_2.show()
//    data_2.write.save("data.csv")
//    val data_3 = HTData_1.select(from_unixtime(col("time")).as("time"),from_unixtime(col("time")).as("date"))
//    val data_3 = HTData_1.select(from_unixtime(to_timestamp(col("time")).as("time")),from_unixtime(to_timestamp(col("time")).as("date")))
//    data_3.show()

    val data_4 = HTData_1.withColumn("time-stamp",from_unixtime(col("time"))).select("name","heartrate","device_id","time-stamp")
    data_4.show()
    val data_5 = HTData_1.where("device_id == 1").select("device_id","heartrate")
    data_5.show()
//    val data_6 = HTData_1.withColumn("avg(HR)",avg("heartrate")).select("device_id","avg(HR)")
//    data_6.show()

    val data_7 = HTData_1.join(HTData_2).groupBy("device_id").agg(count("device_id"))
    data_7.show()
  }
}
